# SteveBot 1.0.0

A Discord bot with essentials.

## Requirements
- discord.js
- nekos.life@1.1.5
- urban
- node-opus
- ytdl-core
- simple-youtube-api
- canvas@1.6.13

## botconfig.json
```json
{
    "TOKEN": "",
    "GOOGLE_API_KEY": "",
    "prefix": "s!",
    "color": "#008000",
    "presence": "bots.danishhumair.com"
}
```